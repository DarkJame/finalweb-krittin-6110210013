using Tutorial.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Tutorial.Data
{
    public class TutorialContext : IdentityDbContext<NewsUser>
    {
        public DbSet<Subject> Subjects {get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Subject.db");
        }
    }
}