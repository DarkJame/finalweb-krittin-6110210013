using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Tutorial.Models
{
    public class Subject
    {
        public int SubjectID{ get; set; }
        public string nameSubject{ get ; set;}
        public int hour { get; set;}
        public double price { get; set;}
        public string location { get; set; }
        public int Numberofparticipants { get; set; }
        public string NewsUserId {get; set;}
        public NewsUser postUser {get; set;}
    }
        public class NewsUser : IdentityUser{
            public string FirstName { get; set;}
            public string LastName { get; set;}
    }

}