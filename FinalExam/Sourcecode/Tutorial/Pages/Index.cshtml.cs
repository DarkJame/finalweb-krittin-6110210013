﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutorial.Models;
using Tutorial.Data;

namespace Tutorial.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Tutorial.Data.TutorialContext _context;
        public IndexModel(Tutorial.Data.TutorialContext context)
        {
            _context = context;
        }
        public IList<Subject> Subject { get; set; }
        public async Task OnGetAsync()
        {
            Subject = await _context.Subjects.ToListAsync();

        }
    }
}
